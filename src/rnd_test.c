/*-
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* Test the KISS99 implementation for Marsaglia's known answer. */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "rnd.h"

#define MWC1 (12345)
#define MWC2 (65435)
#define JSR (34221)
#define JCONG (12345)

#define EXPECT (1372460312U)

int
main(int argc, char *argv[])
{
	uint32_t r, seed[4];

	(void)argc;
	(void)argv;

	seed[0] = MWC1;
	seed[1] = MWC2;
	seed[2] = JSR;
	seed[3] = JCONG;
	rnd_init(seed);

	for (int i = 0; i < 1000255; i++)
		(void)rnd_nxt();
	r = rnd_nxt();
	if (r != EXPECT) {
		fprintf(stderr, "got %" PRIu32 ",expected %" PRIu32 "\n",
			r, EXPECT);
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}
