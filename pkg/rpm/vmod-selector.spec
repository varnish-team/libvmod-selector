# -D MUST pass in _version and _release, and SHOULD pass in dist.

Summary: Varnish VMOD for matching strings associated with backends, regexen and other strings
Name: vmod-selector
Version: %{_version}
Release: %{_release}%{?dist}
License: BSD
Group: System Environment/Daemons
URL: https://code.uplex.de/uplex-varnish/libvmod-selector
Source0: %{name}-%{version}.tar.gz

# varnish from varnish61 at packagecloud
# This is the Requires for VMOD ABI compatibility with VRT >= 13.0.
Requires: varnishd(vrt)%{?_isa} >= 13

BuildRequires: varnish-devel >= 6.6.0
BuildRequires: pkgconfig
BuildRequires: make
BuildRequires: gcc
BuildRequires: python-docutils >= 0.6

# git builds
#BuildRequires: automake
#BuildRequires: autoconf
#BuildRequires: autoconf-archive
#BuildRequires: libtool
#BuildRequires: python-docutils >= 0.6

Provides: vmod-selector, vmod-selector-debuginfo

%description
Varnish Module for matching fixed strings and prefixes, and
associating the matched string with backends, regular expressions and
other strings.

%prep
%setup -q -n %{name}-%{version}

%build

# if this were a git build
# ./autogen.sh

%configure

make -j

%check

make -j check

%install

make install DESTDIR=%{buildroot}

# Only use the version-specific docdir created by %doc below
rm -rf %{buildroot}%{_docdir}

# None of these for fedora/epel
find %{buildroot}/%{_libdir}/ -name '*.la' -exec rm -f {} ';'
find %{buildroot}/%{_libdir}/ -name '*.a' -exec rm -f {} ';'

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/varnish*/vmods/
%{_mandir}/man3/*.3*
%doc README.rst COPYING LICENSE

%post
/sbin/ldconfig

%changelog
* Wed Apr 21 2021 Geoff Simmons <geoff@uplex.de> - %{_version}-%{_release}
- Require VRT 13.0, compatible with Varnish 6.6
- Add .subroutine(), .check_call() and .bool()
- .compile() no longer required, and is deprecated
- Some source code re-org, for smaller sources

* Wed Dec 30 2020 Geoff Simmons <geoff@uplex.de> - 2.5.0-1
- Refactoring with breaking changes
- Faster implementations of .match() and .hasprefix()
- Compatible with VRT >= 12.0 (Varnish >= 6.5.1)
- .compile() is now required in vcl_init
- Various methods may invoke VCL failure on unrecoverable errors.
- Some means have been added to safeguard against VCL failure.
- Added the 'element' parameter, sets may now be used as associative arrays.
- For details see CHANGES.md in the source repository.

* Mon Dec 9 2019 Geoff Simmons <geoff@uplex.de> - 1.3.1-1
- Bugfix object finalization (issue #1)

* Tue Oct 8 2019 Geoff Simmons <geoff@uplex.de> - 1.3.0-1
- Require VRT 10.0, compatible with Varnish 6.3

* Fri Aug 23 2019 Geoff Simmons <geoff@uplex.de> - 1.2.0-1
  Add the integer param to .add(), and the .integer() method.

* Tue Apr 16 2019 Geoff Simmons <geoff@uplex.de> - 1.1.0-1
  Compatibilty with VRT 9 (Varnish 6.2)

* Fri Nov 30 2018 Geoff Simmons <geoff@uplex.de> - 1.0.0-1
  Compatibilty with VRT 8 (Varnish 6.1 and libvarnishapi.so.2)

* Tue Jul 10 2018 Geoff Simmons <geoff@uplex.de> - 0.5.0-1
  Initial package version for compatibility with Varnish 6.0.0.
